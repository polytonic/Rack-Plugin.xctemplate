//
//  SimpleDelay.cpp
//  ___PACKAGENAMEASIDENTIFIER___
//
//  Created by Dragan Petrovic on 10/03/2018.
//  Copyright © 2018 Dragan Petrovic. All rights reserved.
//

#include "SimpleDelay.hpp"
#include <vector>

struct SimpleDelay::Pimpl {
    Pimpl(const Delegate* del) : buffer(96000), bufidx(0), delegate(const_cast<Delegate*>(del)) {
            std::fill(buffer.begin(), buffer.end(), 0);
    }
    
    dsp32_t process(dsp32_t input, int32_t time, dsp32_t feedback) {
        dsp32_t output = buffer.at(bufidx);
        buffer.at(bufidx) = input + (output * feedback);
        if(++bufidx >= time) {
            bufidx = 0;
            if(delegate != NULL) {
                delegate->delegate(this);
            }
        }
        return output;
    }
    
private:
    std::vector<dsp32_t> buffer;
    int32_t bufidx;
    Delegate* delegate;
};

SimpleDelay::SimpleDelay(const Delegate* delegate) : pimpl(new Pimpl(delegate)) {}
SimpleDelay::~SimpleDelay() {}
dsp32_t SimpleDelay::process(dsp32_t input, int32_t time, dsp32_t feedback) {
    return pimpl->process(input, time, feedback);
}
