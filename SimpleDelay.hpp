//
//  SimpleDelay.hpp
//  ___PACKAGENAMEASIDENTIFIER___
//
//  Created by Dragan Petrovic on 10/03/2018.
//  Copyright © 2018 Dragan Petrovic. All rights reserved.
//

#ifndef SimpleDelay_hpp
#define SimpleDelay_hpp

#include "___PACKAGENAMEASIDENTIFIER___Helper.hpp"

struct SimpleDelay {
    SimpleDelay(const Delegate* = NULL);
    ~SimpleDelay();
    dsp32_t process(dsp32_t input, int32_t time, dsp32_t feedback);
    PrivateImplementation;
};

#endif /* SimpleDelay_hpp */
