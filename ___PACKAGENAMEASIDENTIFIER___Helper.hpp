//
//  ___PACKAGENAMEASIDENTIFIER___Helper.hpp
//  Template
//
//  Created by Dragan Petrovic on 10/03/2018.
//  Copyright © 2018 Dragan Petrovic. All rights reserved.
//

#ifndef ___PACKAGENAMEASIDENTIFIER___Helper_hpp
#define ___PACKAGENAMEASIDENTIFIER___Helper_hpp

#include <memory>
#include <math.h>

typedef short int16_t;
typedef int int32_t;
typedef float dsp32_t;
typedef double dsp64_t;                                                                                                                                 

struct Delegate {
    virtual void delegate(void*) = 0;
};

/* Private implementation declaration START */
#define PrivateImplementation   private: \
                                struct Pimpl; \
                                std::unique_ptr<Pimpl> pimpl
/* Private implementation declaration END */

#endif /* ___PACKAGENAMEASIDENTIFIER___Helper_hpp */
