#include "___PACKAGENAMEASIDENTIFIER___.hpp"
#include "___PACKAGENAMEASIDENTIFIER___Helper.hpp"
#include "SimpleDelay.hpp"

struct ___FILEBASENAMEASIDENTIFIER___ : Module, Delegate {
	enum ParamIds {
        TIME_PARAM,
        FEEDBACK_PARAM,
		NUM_PARAMS
	};
	enum InputIds {
		FIRST_INPUT,
		NUM_INPUTS
	};
	enum OutputIds {
		FIRST_OUTPUT,
		NUM_OUTPUTS
	};
	enum LightIds {
		BLINK_LIGHT,
		NUM_LIGHTS
	};

	float blinkPhase = 0.0;

	___FILEBASENAME___() : Module(NUM_PARAMS, NUM_INPUTS, NUM_OUTPUTS, NUM_LIGHTS), delay(this) {}
    
	void step() override;
    void delegate(void*) override;
    
	// For more advanced Module features, read Rack's engine.hpp header file
	// - toJson, fromJson: serialization of internal data
	// - onSampleRateChange: event triggered by a change of sample rate
	// - onReset, onRandomize, onCreate, onDelete: implements special behavior when user clicks these from the context menu
private:
    SimpleDelay delay;
    bool blink;
};

#pragma mark - Step
void ___FILEBASENAMEASIDENTIFIER___::step() {
    outputs[FIRST_OUTPUT].value = delay.process(inputs[FIRST_INPUT].value,
                                                params[TIME_PARAM].value,
                                                params[FEEDBACK_PARAM].value);
}

#pragma mark - Delegate
void ___FILEBASENAMEASIDENTIFIER___::delegate(void* input) {
    blink = !blink;
    lights[BLINK_LIGHT].value = blink ? 1.0 : 0.0;
}

#pragma mark - Interface
struct OpaqueDisplay : OpaqueWidget {
    /* This one is for example purpose */
    ___FILEBASENAMEASIDENTIFIER___ *module;
    void draw(NVGcontext *vg) override {
        nvgFillColor(vg, nvgRGBA(0xff, 0x00, 0x00, 0x50));
        {
            nvgBeginPath(vg);
            nvgMoveTo(vg, 0, 0);
            nvgLineTo(vg, 0, box.size.y);
            nvgLineTo(vg, box.size.x, 0);
            nvgClosePath(vg);
        }
        nvgFill(vg);
    }
};

___PACKAGENAMEASIDENTIFIER___Widget::___PACKAGENAMEASIDENTIFIER___Widget() {
	___FILEBASENAMEASIDENTIFIER___ *module = new ___FILEBASENAMEASIDENTIFIER___();
	setModule(module);
	box.size = Vec(6 * RACK_GRID_WIDTH, RACK_GRID_HEIGHT);
	{
		SVGPanel *panel = new SVGPanel();
		panel->box.size = box.size;
		panel->setBackground(SVG::load(assetPlugin(plugin, "res/___FILEBASENAME___.svg")));
		addChild(panel);
	}
    
    { /* This one is for example purpose */
        OpaqueDisplay *display = new OpaqueDisplay();
        display->module = module;
        display->box.pos = Vec(0, 0);
        display->box.size = Vec(box.size.x, box.size.y);
        addChild(display);
    }

	addChild(createScrew<ScrewSilver>(Vec(RACK_GRID_WIDTH, 0)));
	addChild(createScrew<ScrewSilver>(Vec(box.size.x - 2 * RACK_GRID_WIDTH, 0)));
	addChild(createScrew<ScrewSilver>(Vec(RACK_GRID_WIDTH, RACK_GRID_HEIGHT - RACK_GRID_WIDTH)));
	addChild(createScrew<ScrewSilver>(Vec(box.size.x - 2 * RACK_GRID_WIDTH, RACK_GRID_HEIGHT - RACK_GRID_WIDTH)));

    addInput(createInput<PJ301MPort>(Vec(33, 186), module, ___FILEBASENAME___::FIRST_INPUT));
    addParam(createParam<Davies1900hBlackKnob>(Vec(28, 87), module, ___FILEBASENAME___::TIME_PARAM, 100, 44100, 1000));
    addParam(createParam<Davies1900hBlackKnob>(Vec(28, 130), module, ___FILEBASENAME___::FEEDBACK_PARAM, -1, 1, 0.5));
    addOutput(createOutput<PJ301MPort>(Vec(33, 275), module, ___FILEBASENAME___::FIRST_OUTPUT));
	addChild(createLight<MediumLight<RedLight>>(Vec(41, 59), module, ___FILEBASENAME___::BLINK_LIGHT));
}
